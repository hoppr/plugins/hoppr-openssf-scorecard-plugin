POETRY ?= poetry

BLACK ?= ${POETRY} run black
MYPY ?= ${POETRY} run mypy
PYLINT ?= ${POETRY} run pylint
PYTEST ?= ${POETRY} run pytest
SOURCERY ?= ${POETRY} run sourcery

TARGETS := hoppr_openssf_scorecard test/unit

.PHONY: all build clean format test
.SILENT: clean

all: clean format test build

build: clean
	${POETRY} lock
	${POETRY} install --sync
	${POETRY} build

clean:
	${RM} -r dist .coverage* *.link .*.link-unfinished hopctl-merge-*.json
	find ${PWD} -name "*.log" -exec ${RM} {} \;

format: black-fix sourcery-fix

test: black pylint mypy pytest sourcery

black:
	${BLACK} --check ${TARGETS}

black-fix:
	${BLACK} ${TARGETS}

pylint:
	${PYLINT} ${TARGETS}

mypy:
	${MYPY}

pytest:
	${PYTEST}

sourcery:
	${SOURCERY} review --check --verbose ${TARGETS}

sourcery-fix:
	${SOURCERY} review --fix --verbose ${TARGETS}
