"""
Test module for the GolangScorecardHelper class
"""

from __future__ import annotations

import re

from typing import Callable

import httpx
import pytest

from hoppr import CredentialRequiredService, Credentials
from pytest import MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._golang import GolangScorecardHelper
from hoppr_openssf_scorecard._helpers._libraries import LibrariesIOScorecardHelper


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically set environment
    """
    monkeypatch.setenv(name="LIBRARIES_API_KEY", value="TEST_LIBRARIES_API_KEY")


@pytest.mark.parametrize(
    argnames=["golang_proxy_response", "libraries_response", "expected_result"],
    argvalues=[
        ("https://golang.google.org/hoppr/test", "https://github.com/hoppr/test", ["https://github.com/hoppr/test"]),
        (None, None, []),
    ],
)
async def test_get_vcs_repo_url(
    helper: GolangScorecardHelper,
    golang_proxy_response: str | None,
    libraries_response: str | None,
    expected_result: list[str],
    httpx_mock: HTTPXMock,
):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        json={"Origin": {"URL": golang_proxy_response}},
    )

    httpx_mock.add_response(
        url=re.compile(pattern="^https://libraries.io/api/.*$"),
        status_code=200,
        json={"repository_url": libraries_response},
    )

    result = await helper.get_vcs_repo_url("pkg:golang/hoppr-test.com/hoppr/test-project@1.2.3")
    assert result == expected_result


async def test_get_vcs_repo_url_no_api_key(helper: GolangScorecardHelper, monkeypatch: MonkeyPatch):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method raises EnvironmentError
    """
    # pylint: disable=duplicate-code

    monkeypatch.delenv(name="LIBRARIES_API_KEY", raising=False)

    with pytest.raises(
        expected_exception=EnvironmentError,
        match=(
            "Either a credentials file with an entry for 'https://libraries.io/api' "
            "or the environment variable LIBRARIES_API_KEY must be set to use this plugin."
        ),
    ):
        await helper.get_vcs_repo_url("")


async def test_get_vcs_repo_url_not_found_error(helper: GolangScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url("pkg:golang/hoppr-test.com/hoppr/test-project@1.2.3")
        assert result == []


async def test_get_vcs_repo_url_github_purl_namespace(helper: GolangScorecardHelper):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method if supplied purl namespace contains `github.com`
    """
    result = await helper.get_vcs_repo_url("pkg:golang/github.com/hoppr/test-project@1.2.3")
    assert result == ["https://github.com/hoppr/test-project"]


async def test_get_vcs_repo_url_timeout(helper: GolangScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url(purl_string="pkg:golang/hoppr-test.com/hoppr/test-project@1.2.3")
        assert result is None


@pytest.mark.parametrize(argnames="cred_object", argvalues=[{"url": LibrariesIOScorecardHelper.API_URL}], indirect=True)
async def test_get_vcs_repo_url_with_credentials(
    helper: GolangScorecardHelper,
    find_credentials: Callable[[str], CredentialRequiredService],
    monkeypatch: MonkeyPatch,
    httpx_mock: HTTPXMock,
):
    """
    Test GolangScorecardHelper.get_vcs_repo_url method with parsed Credentials input
    """
    # pylint: disable=duplicate-code

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials)

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        json={"Origin": {"URL": "https://golang.google.org/hoppr/test"}},
    )

    httpx_mock.add_response(
        url=re.compile(pattern="^https://libraries.io/api/.*$"),
        status_code=200,
        json={"repository_url": "https://github.com/hoppr/test-project"},
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:golang/hoppr-test.com/hoppr/test-project@1.2.3")
    assert result == ["https://github.com/hoppr/test-project"]
