"""
Test module for the HelmScorecardHelper class
"""

from __future__ import annotations

import re

import httpx
import pytest

from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._helm import HelmScorecardHelper


@pytest.mark.parametrize(
    argnames=["response_repository", "expected_result"],
    argvalues=[
        ("https://github.com/hoppr/test-chart", ["https://github.com/hoppr/test-chart"]),
        ("https://gitbox.com/hoppr/test-chart", []),
    ],
)
async def test_get_vcs_repo_url(
    helper: HelmScorecardHelper, response_repository: str, expected_result: list[str], httpx_mock: HTTPXMock
):
    """
    Test HelmScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        json={"home_url": response_repository},
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:helm/hoppr/test-chart@1.2.3")
    assert result == expected_result


async def test_get_vcs_repo_url_not_found_error(helper: HelmScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test HelmScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url(purl_string="pkg:helm/hoppr/test-chart@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: HelmScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test HelmScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    # pylint: disable=duplicate-code

    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url(purl_string="pkg:helm/hoppr/test-chart@1.2.3")
        assert result is None
