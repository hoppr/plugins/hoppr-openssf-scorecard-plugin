"""
Test module for the GitHubScorecardHelper class
"""

from __future__ import annotations

import re

from datetime import datetime
from typing import Any

import httpx
import pytest

from pytest import FixtureRequest, MonkeyPatch
from pytest_httpx import HTTPXMock
from trio.testing import MockClock

from hoppr_openssf_scorecard._helpers._github import GitHubScorecardHelper


@pytest.fixture(name="response", scope="function")
def response_fixture(request: FixtureRequest) -> dict[str, Any]:
    """
    Fixture to return mocked response from GitHub API
    """
    names = getattr(request, "param", ["test-project", "another-test"])

    response: dict[str, list[dict[str, Any]]] = {"items": []}

    for idx, name in enumerate(names):
        response["items"].append(
            {
                "name": name,
                "full_name": f"hoppr/{name}",
                "forks": idx,
                "watchers": idx,
            }
        )

    return response


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically patch sleep methods
    """
    monkeypatch.setattr(target="hoppr_openssf_scorecard._helpers._github.sleep", name=lambda secs: None)


async def _mock_await_rate_limit_reset():
    return None


async def test_await_rate_limit_reset(helper: GitHubScorecardHelper, httpx_mock: HTTPXMock, mock_clock: MockClock):
    """
    Test GitHubScorecardHelper._await_rate_limit_reset method
    """
    reset_time = int(datetime.now().timestamp() + 10)

    httpx_mock.add_response(
        url=f"{helper.API_URL}/rate_limit",
        status_code=200,
        json={"resources": {"search": {"remaining": 0, "reset": reset_time}}},
    )

    mock_clock.rate = 10.0

    await helper.await_rate_limit_reset()


@pytest.mark.parametrize(
    argnames=["response", "purl_string", "expected_result"],
    argvalues=[
        (["test-pypi-pkg", "another-test"], "pkg:pypi/test-pypi-pkg@1.2.3", ["https://github.com/hoppr/test-pypi-pkg"]),
        (["test-mvn-pkg", "another-test"], "pkg:maven/test-mvn-pkg@1.2.3", ["https://github.com/hoppr/test-mvn-pkg"]),
        (["test-go-pkg", "another-test"], "pkg:golang/test-go-pkg@1.2.3", ["https://github.com/hoppr/test-go-pkg"]),
        (["test-npm-pkg", "another-test"], "pkg:npm/test-npm-pkg@1.2.3", ["https://github.com/hoppr/test-npm-pkg"]),
        (["test-npm-pkg", "another-test"], "pkg:npm/test-npm-pkg@1.2.3", ["https://github.com/hoppr/test-npm-pkg"]),
        (["test-rpm-pkg", "another-test"], "pkg:rpm/test-rpm-pkg@1.2.3", ["https://github.com/hoppr/test-rpm-pkg"]),
        (["another-test", "yet-another-test"], "pkg:pypi/test-pypi-pkg@1.2.3", []),
        ([], "pkg:pypi/test-pypi-pkg@1.2.3", []),
    ],
    indirect=["response"],
)
async def test_get_vcs_repo_url(  # pylint: disable=too-many-arguments
    helper: GitHubScorecardHelper,
    response: dict[str, Any],
    purl_string: str,
    expected_result: list[str],
    httpx_mock: HTTPXMock,
    monkeypatch: MonkeyPatch,
):
    """
    Test GitHubScorecardHelper.get_vcs_repo_url method
    """
    monkeypatch.setattr(target=helper, name="await_rate_limit_reset", value=_mock_await_rate_limit_reset)

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/search/repositories.*$"),
        status_code=200,
        json=response,
    )

    result = await helper.get_vcs_repo_url(purl_string)
    assert result == expected_result


async def test_get_vcs_repo_url_not_found_error(
    helper: GitHubScorecardHelper, httpx_mock: HTTPXMock, monkeypatch: MonkeyPatch
):
    """
    Test GitHubScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    monkeypatch.setattr(target=helper, name="await_rate_limit_reset", value=_mock_await_rate_limit_reset)

    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/search/repositories.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: GitHubScorecardHelper, httpx_mock: HTTPXMock, monkeypatch: MonkeyPatch):
    """
    Test GitHubScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    monkeypatch.setattr(target=helper, name="await_rate_limit_reset", value=_mock_await_rate_limit_reset)

    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/search/repositories.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None
