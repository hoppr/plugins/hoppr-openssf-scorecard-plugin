"""
Test module for the DebScorecardHelper class
"""

from __future__ import annotations

import re

from typing import Any

import pytest

from packageurl import PackageURL
from pytest import MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._deb import DebScorecardHelper


@pytest.mark.parametrize(
    argnames=["search_response", "src_response", "info_response", "expected_result"],
    argvalues=[
        (
            {"results": {"exact": {"name": "test-deb-pkg"}}},
            {"versions": [{"version": "1.2.3"}]},
            {"pkg_infos": {"vcs_browser": "https://github.com/hoppr/test-deb-pkg"}},
            ["https://github.com/hoppr/test-deb-pkg"],
        ),
        (
            {"results": {"exact": None, "other": [{"name": "test-deb-pkg"}]}},
            {"versions": [{"version": "1.2.3"}]},
            {"pkg_infos": {"vcs_browser": "https://github.com/hoppr/test-deb-pkg"}},
            ["https://github.com/hoppr/test-deb-pkg"],
        ),
        (
            {"results": {"exact": None, "other": [{"name": "test-deb-pkg"}]}},
            {"versions": [{"version": "1.2.3"}]},
            {"pkg_infos": {"vcs_browser": "https://gitbox.com/hoppr/test-deb-pkg"}},
            [],
        ),
    ],
)
async def test__query_debian_api(  # pylint: disable=too-many-arguments
    helper: DebScorecardHelper,
    search_response: dict[str, Any],
    src_response: dict[str, Any],
    info_response: dict[str, Any],
    expected_result: list[str],
    httpx_mock: HTTPXMock,
):
    """
    Test DebScorecardHelper._query_debian_api method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.DEBIAN_API_URL}/search/.*$"),
        status_code=200,
        json=search_response,
    )

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.DEBIAN_API_URL}/src/.*$"),
        status_code=200,
        json=src_response,
    )

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.DEBIAN_API_URL}/info/package/.*$"),
        status_code=200,
        json=info_response,
    )

    purl: PackageURL = PackageURL.from_string("pkg:deb/debian/test-deb-pkg@1.2.3")  # pyright: ignore

    result = await helper._query_debian_api(purl)  # pylint: disable=protected-access
    assert result == expected_result


@pytest.mark.parametrize(
    argnames=["src_response", "upstream_product_response", "expected_result"],
    argvalues=[
        (
            {"upstream_product_link": "https://api.launchpad.net/1.0/upstream-test-deb-pkg"},
            {"homepage_url": "https://github.com/hoppr/test-deb-pkg"},
            ["https://github.com/hoppr/test-deb-pkg"],
        ),
        (
            {"upstream_product_link": "https://api.launchpad.net/1.0/upstream-test-deb-pkg"},
            {"homepage_url": "https://gitbox.com/hoppr/test-deb-pkg"},
            [],
        ),
        ({}, {}, []),
    ],
)
async def test__query_launchpad_api(
    helper: DebScorecardHelper,
    src_response: dict[str, Any],
    upstream_product_response: dict[str, Any],
    expected_result: list[str],
    httpx_mock: HTTPXMock,
):
    """
    Test DebScorecardHelper._query_launchpad_api method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.LAUNCHPAD_API_URL}/ubuntu/\\+source/.*$"),
        status_code=200,
        json=src_response,
    )

    if src_response.get("upstream_product_link") is not None:
        httpx_mock.add_response(
            url=src_response.get("upstream_product_link"),
            status_code=200,
            json=upstream_product_response,
        )

    purl: PackageURL = PackageURL.from_string("pkg:deb/ubuntu/test-deb-pkg@1.2.3")  # pyright: ignore

    result = await helper._query_launchpad_api(purl)  # pylint: disable=protected-access
    assert result == expected_result


@pytest.mark.parametrize(
    argnames=["purl_string", "repo_url", "expected_result"],
    argvalues=[
        (
            "pkg:deb/debian/test-deb-pkg@1.2.3",
            "https://github.com/hoppr/test-deb-pkg",
            ["https://github.com/hoppr/test-deb-pkg"],
        ),
        (
            "pkg:deb/ubuntu/test-deb-pkg@1.2.3",
            "https://github.com/hoppr/test-deb-pkg",
            ["https://github.com/hoppr/test-deb-pkg"],
        ),
    ],
)
async def test_get_vcs_repo_url(
    helper: DebScorecardHelper, purl_string: str, repo_url: str, expected_result: list[str], monkeypatch: MonkeyPatch
):
    """
    Test DebScorecardHelper.get_vcs_repo_url method
    """

    async def _mock_query_api(purl: PackageURL) -> list[str]:  # pylint: disable=unused-argument
        return [repo_url]

    monkeypatch.setattr(target=helper, name="_query_debian_api", value=_mock_query_api)
    monkeypatch.setattr(target=helper, name="_query_launchpad_api", value=_mock_query_api)

    assert await helper.get_vcs_repo_url(purl_string) == expected_result


async def test_get_vcs_repo_url_bad_distro(helper: DebScorecardHelper):
    """
    Test DebScorecardHelper.get_vcs_repo_url method raises ValueError given wrong PURL namespace
    """
    with pytest.raises(
        expected_exception=ValueError,
        match="Namespace for 'pkg:deb' PURL must be one of 'debian', 'ubuntu'",
    ):
        await helper.get_vcs_repo_url("pkg:deb/elementary/test-deb-pkg@1.2.3")
