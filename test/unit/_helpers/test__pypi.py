"""
Test module for the PyPIScorecardHelper class
"""

from __future__ import annotations

import re

import httpx
import pytest

from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._pypi import PyPIScorecardHelper


@pytest.mark.parametrize(
    argnames=["response", "expected_result"],
    argvalues=[
        (
            {"info": {"home_page": "https://github.com/hoppr/test-pypi-pkg/tree/main"}},
            ["https://github.com/hoppr/test-pypi-pkg"],
        ),
        (
            {"info": {"project_urls": {"Homepage": "https://github.com/hoppr/test-pypi-pkg/tree/main"}}},
            ["https://github.com/hoppr/test-pypi-pkg"],
        ),
        (
            {"info": {"project_urls": {"Source": "https://github.com/hoppr/test-pypi-pkg/tree/main"}}},
            ["https://github.com/hoppr/test-pypi-pkg"],
        ),
        (
            {"info": {"home_page": "https://gitbox.com/hoppr/test-pypi-pkg/tree/main"}},
            [],
        ),
    ],
)
async def test_get_vcs_repo_url(
    helper: PyPIScorecardHelper, response: str, expected_result: list[str], httpx_mock: HTTPXMock
):
    """
    Test PyPIScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/pypi/.*/json$"),
        status_code=200,
        json=response,
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:pypi/test-pypi-pkg@1.2.3")
    assert result == expected_result


async def test_get_vcs_repo_url_not_found_error(helper: PyPIScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test PyPIScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/pypi/.*/json$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url(purl_string="pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None


async def test_get_vcs_repo_url_timeout(helper: PyPIScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test PyPIScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/pypi/.*/json$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url("pkg:pypi/test-pypi-pkg@1.2.3")
        assert result is None
