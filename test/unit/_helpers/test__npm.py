"""
Test module for the NPMScorecardHelper class
"""

from __future__ import annotations

import re

from typing import Type, TypeVar

import httpx
import pytest

from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._npm import NPMScorecardHelper


ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.mark.parametrize(
    argnames=["response_status_code", "response_repository", "expected_exception", "expected_result"],
    argvalues=[
        (200, {"type": "git", "url": "https://github.com/hoppr/hoppr.git"}, None, ["https://github.com/hoppr/hoppr"]),
        (200, {"type": "git", "url": "git@github.com:hoppr/hoppr.git"}, None, ["https://github.com/hoppr/hoppr"]),
        (200, {}, None, []),
        (404, {}, httpx.HTTPStatusError, []),
    ],
)
async def test_get_vcs_repo_url(  # pylint: disable=too-many-arguments
    helper: NPMScorecardHelper,
    response_status_code: int,
    response_repository: dict[str, str],
    expected_exception: Type[ExpectedException] | None,
    expected_result: list[str],
    httpx_mock: HTTPXMock,
):
    """
    Test NPMScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=response_status_code,
        json={"repository": response_repository},
    )

    if expected_exception is not None:
        with pytest.raises(expected_exception):
            result = await helper.get_vcs_repo_url(purl_string="pkg:npm/@hoppr/test-purl@1.2.3")
            assert result is None
    else:
        result = await helper.get_vcs_repo_url(purl_string="pkg:npm/@hoppr/test-purl@1.2.3")
        assert result == expected_result


async def test_get_vcs_repo_url_timeout(helper: NPMScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test NPMScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url(purl_string="pkg:npm/@hoppr/test-purl@1.2.3")
        assert result is None
