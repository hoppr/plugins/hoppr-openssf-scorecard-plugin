"""
Test module for the MavenScorecardHelper class
"""

from __future__ import annotations

import re

from typing import Callable

import httpx
import pytest

from hoppr import CredentialRequiredService, Credentials
from pytest import MonkeyPatch
from pytest_httpx import HTTPXMock

from hoppr_openssf_scorecard._helpers._libraries import LibrariesIOScorecardHelper
from hoppr_openssf_scorecard._helpers._maven import MavenScorecardHelper


@pytest.fixture(scope="function", autouse=True)
def setup_fixture(monkeypatch: MonkeyPatch):
    """
    Pre-test fixture to automatically set environment
    """
    monkeypatch.setenv(name="LIBRARIES_API_KEY", value="TEST_LIBRARIES_API_KEY")


@pytest.mark.parametrize(
    argnames=["response_repository", "expected_result"],
    argvalues=[
        ("https://github.com/hoppr/test-repo", ["https://github.com/hoppr/test-repo"]),
        ("https://gitbox.com/hoppr/test-repo", []),
    ],
)
async def test_get_vcs_repo_url(
    helper: MavenScorecardHelper, response_repository: str, expected_result: list[str], httpx_mock: HTTPXMock
):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        text=f"<project><scm><connection>{response_repository}</connection></scm></project>",
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:maven/com.hoppr.test/hoppr-test@1.2.3?type=jar")
    assert result == expected_result


@pytest.mark.parametrize(
    argnames=["response_repository", "expected_result"],
    argvalues=[
        ("https://github.com/hoppr/test-repo", ["https://github.com/hoppr/test-repo"]),
        ("https://gitbox.apache.org/repos/asf/test-repo.git", ["https://github.com/apache/test-repo"]),
        (None, []),
    ],
)
async def test_get_vcs_repo_url_no_project_scm_url(
    helper: MavenScorecardHelper, response_repository: str, expected_result: list[str], httpx_mock: HTTPXMock
):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method when no `project.scm.url` data returned in response
    """
    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        text="<project></project>",
    )

    httpx_mock.add_response(
        url=re.compile(pattern="^https://libraries.io/api/.*$"),
        status_code=200,
        json={"repository_url": response_repository},
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:maven/com.hoppr.test/hoppr-test@1.2.3?type=jar")
    assert result == expected_result


async def test_get_vcs_repo_url_no_api_key(helper: MavenScorecardHelper, monkeypatch: MonkeyPatch):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method raises EnvironmentError
    """
    # pylint: disable=duplicate-code

    monkeypatch.delenv(name="LIBRARIES_API_KEY", raising=False)

    with pytest.raises(
        expected_exception=EnvironmentError,
        match=(
            "Either a credentials file with an entry for 'https://libraries.io/api' "
            "or the environment variable LIBRARIES_API_KEY must be set to use this plugin."
        ),
    ):
        await helper.get_vcs_repo_url("")


async def test_get_vcs_repo_url_not_found_error(helper: MavenScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method raises httpx.HTTPStatusError on 404 response
    """
    httpx_mock.add_response(url=re.compile(pattern=f"^{helper.API_URL}/.*$"), status_code=404)

    with pytest.raises(expected_exception=httpx.HTTPStatusError):
        result = await helper.get_vcs_repo_url(purl_string="pkg:maven/com.hoppr.test/hoppr-test@1.2.3?type=jar")

        assert result is None


async def test_get_vcs_repo_url_timeout(helper: MavenScorecardHelper, httpx_mock: HTTPXMock):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method returns None if timeout occurs
    """
    # pylint: disable=duplicate-code

    httpx_mock.add_exception(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        exception=httpx.TimeoutException(message="TIMEOUT"),
    )

    with pytest.raises(expected_exception=httpx.TimeoutException):
        result = await helper.get_vcs_repo_url(purl_string="pkg:maven/com.hoppr.test/hoppr-test@1.2.3?type=jar")

        assert result is None


@pytest.mark.parametrize(argnames="cred_object", argvalues=[{"url": LibrariesIOScorecardHelper.API_URL}], indirect=True)
async def test_get_vcs_repo_url_with_credentials(
    helper: MavenScorecardHelper,
    find_credentials: Callable[[str], CredentialRequiredService],
    monkeypatch: MonkeyPatch,
    httpx_mock: HTTPXMock,
):
    """
    Test MavenScorecardHelper.get_vcs_repo_url method with parsed Credentials input
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials)

    httpx_mock.add_response(
        url=re.compile(pattern=f"^{helper.API_URL}/.*$"),
        status_code=200,
        text="<project></project>",
    )

    httpx_mock.add_response(
        url=re.compile(pattern="^https://libraries.io/api/.*$"),
        status_code=200,
        json={"repository_url": "https://github.com/hoppr/test-mvn-pkg"},
    )

    result = await helper.get_vcs_repo_url(purl_string="pkg:maven/com.hoppr.test/hoppr-test@1.2.3?type=jar")
    assert result == ["https://github.com/hoppr/test-mvn-pkg"]
