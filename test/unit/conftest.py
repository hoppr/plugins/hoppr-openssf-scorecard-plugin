"""
Shareable fixtures for unit tests
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import json
import test
import typing

from pathlib import Path
from subprocess import CompletedProcess
from threading import _RLock as RLock
from typing import Any, Callable, Type

import pytest

from hoppr import CredentialRequiredService, HopprContext, HopprPlugin, Manifest, Sbom
from pytest import FixtureRequest, MonkeyPatch, TempPathFactory

from hoppr_openssf_scorecard._helpers._base import BaseScorecardHelper


@pytest.fixture(autouse=True)
def print_test_name(request: FixtureRequest):
    """
    Print name of test
    """
    print(f" Starting {request.node.name} ".center(120, "-"))


@pytest.fixture(name="collect_root_dir", scope="function")
def collect_root_dir_fixture(request: FixtureRequest, tmp_path_factory: TempPathFactory) -> Path:
    """
    Parametrizable temp dir fixture
    """
    basename = getattr(request, "param", "hoppr_unit_test")
    return tmp_path_factory.mktemp(basename)


@pytest.fixture(name="completed_process")
def completed_process_fixture(request: FixtureRequest) -> CompletedProcess:
    """
    CompletedProcess fixture for subprocess commands

    Intended for use with `pytest` test modules. For `unittest.TestCase` based
    classes, use the `test.mock_objects.MockSubprocessRun` mock object.
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["args"] = param_dict.get("args", ["mock", "command"])
    param_dict["returncode"] = param_dict.get("returncode", 0)
    param_dict["stdout"] = param_dict.get("stdout", b"Mock stdout")
    param_dict["stderr"] = param_dict.get("stderr", b"Mock stderr")

    return CompletedProcess(**param_dict)


@pytest.fixture(name="config")
def config_fixture(request: FixtureRequest) -> dict[str, str]:
    """
    Test plugin config fixture
    """
    return dict(getattr(request, "param", {}))


@pytest.fixture(name="context")
def context_fixture(request: FixtureRequest, manifest: Manifest, collect_root_dir: Path) -> HopprContext:
    """
    Test Context fixture
    """
    sbom_path = Path(test.__file__).parent / "resources" / "sbom.json"
    strict_repos = True

    if hasattr(request, "cls") and request.cls is not None:
        sbom_path = getattr(request.cls, "sbom_path", sbom_path)
        strict_repos = getattr(request.cls, "strict_repos", strict_repos)
    else:
        param_dict = dict(getattr(request, "param", {}))
        sbom_path = param_dict.get("sbom_path", sbom_path)
        strict_repos = param_dict.get("strict_repos", strict_repos)

    with sbom_path.open(mode="r", encoding="utf-8") as bom_file:
        bom_dict = json.load(fp=bom_file)

    sbom = Sbom(**bom_dict)

    return HopprContext(
        collect_root_dir=collect_root_dir,
        consolidated_sbom=sbom,
        credential_required_services=None,
        delivered_sbom=sbom.copy(deep=True),
        logfile_lock=RLock(),
        max_processes=3,
        repositories=manifest.repositories,
        sboms=list(Sbom.loaded_sboms.values()),
        stages=[],
    )


@pytest.fixture(name="cred_object")
def cred_object_fixture(request: FixtureRequest, monkeypatch: MonkeyPatch) -> CredentialRequiredService:
    """
    Test CredObject fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["url"] = param_dict.get("url", "https://test.hoppr.com")
    param_dict["user"] = param_dict.get("user", "mock_user")
    param_dict["user_env"] = param_dict.get("pass_env", "MONKEYPATCH_USER_ENV")
    param_dict["pass_env"] = param_dict.get("pass_env", "MONKEYPATCH_PASS_ENV")
    param_dict["password"] = param_dict.get("password", "mock_password")

    monkeypatch.setenv(name=param_dict["user_env"], value=param_dict["user"])
    monkeypatch.setenv(name=param_dict["pass_env"], value=param_dict["password"])

    return CredentialRequiredService.parse_obj(param_dict)


@pytest.fixture(name="find_credentials")
def find_credentials_fixture(
    cred_object: CredentialRequiredService,
) -> Callable[[str], CredentialRequiredService]:
    """
    Fixture to use when monkeypatching `Credentials.find` method
    """

    def _find_credentials(url: str) -> CredentialRequiredService:
        return cred_object

    return _find_credentials


@pytest.fixture(name="helper", scope="function")
def helper_fixture(request: FixtureRequest, context: HopprContext) -> BaseScorecardHelper:
    """
    Fixture to return BaseScorecardHelper instance

    The class type of helper instance returned is determined from the annotated
    type hint of the test function's `helper` argument, if it is a subclass of
    `BaseScorecardHelper`. For example:

    ```python
    def test_get_vcs_repo_url(helper: PyPIScorecardHelper):
        ...
    ```

    will provide a `helper` argument with a type of `PyPIScorecardHelper`.
    """
    # Get type of plugin to return from test function's type hints
    helper_cls: Type[BaseScorecardHelper] = typing.get_type_hints(request.function)["helper"]

    try:
        assert issubclass(helper_cls, BaseScorecardHelper)
    except TypeError as ex:
        raise TypeError(f"helper_fixture: not a subclass of BaseScorecardHelper: '{helper_cls}'") from ex

    return helper_cls(context)


@pytest.fixture(name="libraries_io_response")
def libraries_io_response_fixture(request: FixtureRequest) -> list[dict[str, Any]]:
    """
    Fixture to return simulated Libraries.io response
    """
    names = getattr(request, "param", ["test-pypi-pkg", "another-test"])

    return [
        {
            "dependent_repos_count": 0,
            "dependents_count": 0,
            "deprecation_reason": None,
            "description": "",
            "forks": idx,
            "homepage": "",
            "keywords": [],
            "language": "",
            "latest_download_url": None,
            "latest_release_number": "",
            "latest_release_published_at": "",
            "latest_stable_release_number": "",
            "latest_stable_release_published_at": "",
            "license_normalized": False,
            "licenses": "",
            "name": name,
            "normalized_licenses": [],
            "package_manager_url": "",
            "platform": "",
            "rank": idx,
            "repository_license": "",
            "repository_status": None,
            "repository_url": f"https://github.com/hoppr/{name}",
            "stars": idx,
            "status": None,
            "versions": [],
        }
        for idx, name in enumerate(names, start=1)
    ]


@pytest.fixture(name="manifest")
def manifest_fixture(request: FixtureRequest) -> Manifest:
    """
    Test Manifest fixture
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest.yml"

    if hasattr(request, "cls") and request.cls is not None:
        manifest_path = getattr(request.cls, "manifest_path", manifest_path)
    else:
        param_dict = dict(getattr(request, "param", {}))
        manifest_path = param_dict.get("manifest_path", manifest_path)

    return Manifest.load(manifest_path)


@pytest.fixture(name="plugin")
def plugin_fixture(request: FixtureRequest, config: dict[str, str] | None, context: HopprContext) -> HopprPlugin:
    """
    Test collector plugin fixture

    The class type of Hoppr plugin instance returned is determined from the annotated
    type hint of the test function's `plugin` argument, if it is a subclass of
    `HopprPlugin`. For example:

    ```python
    def test_process_component(plugin: HopprScorecardPlugin):
        ...
    ```

    will provide a `plugin` argument with a type of `HopprScorecardPlugin`.
    """
    # Get type of plugin to return from test function's type hints
    plugin_cls: Type[HopprPlugin] = typing.get_type_hints(request.function)["plugin"]

    try:
        assert issubclass(plugin_cls, HopprPlugin)
    except TypeError as ex:
        raise TypeError(f"plugin_fixture: not a subclass of HopprPlugin: '{plugin_cls}'") from ex

    return plugin_cls(context, config)


@pytest.fixture(name="run_command")
def run_command_fixture(
    completed_process: CompletedProcess,
) -> Callable[[list[str], list[str] | None, str | None], CompletedProcess]:
    """
    Override completed_process to return Callable
    """

    def _run_command(
        command: list[str], password_list: list[str] | None = None, cwd: str | None = None
    ) -> CompletedProcess:
        return completed_process

    return _run_command
