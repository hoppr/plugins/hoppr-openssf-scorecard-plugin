"""
Hoppr OpenSSF Scorecard plugin integration tests
"""
from __future__ import annotations

import json
import sys
import tarfile

from pathlib import Path
from typing import Any

import jmespath
import requests
import rich

from rich.box import Box
from rich.table import Table

from hoppr_openssf_scorecard.models.scorecard import ScorecardResponse


COLUMN_WIDTH = 46

# Mapping of delivered SBOM property names to JMESPath expressions used to search the associated Scorecard response data
PROPERTY_MAP = {
    "date": "date",
    "repo:name": "repo.name",
    "repo:commit": "repo.commit",
    "scorecard:version": "scorecard.version",
    "scorecard:commit": "scorecard.commit",
    "score": "score",
    "check:Binary-Artifacts": "checks[? name == 'Binary-Artifacts'] | [0].score",
    "check:Branch-Protection": "checks[? name == 'Branch-Protection'] | [0].score",
    "check:CI-Tests": "checks[? name == 'CI-Tests'] | [0].score",
    "check:CII-Best-Practices": "checks[? name == 'CII-Best-Practices'] | [0].score",
    "check:Code-Review": "checks[? name == 'Code-Review'] | [0].score",
    "check:Contributors": "checks[? name == 'Contributors'] | [0].score",
    "check:Dangerous-Workflow": "checks[? name == 'Dangerous-Workflow'] | [0].score",
    "check:Dependency-Update-Tool": "checks[? name == 'Dependency-Update-Tool'] | [0].score",
    "check:Fuzzing": "checks[? name == 'Fuzzing'] | [0].score",
    "check:License": "checks[? name == 'License'] | [0].score",
    "check:Maintained": "checks[? name == 'Maintained'] | [0].score",
    "check:Packaging": "checks[? name == 'Packaging'] | [0].score",
    "check:Pinned-Dependencies": "checks[? name == 'Pinned-Dependencies'] | [0].score",
    "check:SAST": "checks[? name == 'SAST'] | [0].score",
    "check:Security-Policy": "checks[? name == 'Security-Policy'] | [0].score",
    "check:Signed-Releases": "checks[? name == 'Signed-Releases'] | [0].score",
    "check:Token-Permissions": "checks[? name == 'Token-Permissions'] | [0].score",
    "check:Vulnerabilities": "checks[? name == 'Vulnerabilities'] | [0].score",
    "check:Webhooks": "checks[? name == 'Webhooks'] | [0].score",
}


def check_result(purl_string: str, delivered_sbom: dict[str, Any], repo_url: str) -> list[str]:
    """
    Check component properties in delivered SBOM against direct response from Scorecard API

    Args:
        purl_string (str): The PURL of the component
        delivered_sbom (dict[str, Any]): The dict representation of the SBOM modified by the plugin
        repo_url (str): Pre-determined VCS repo URL of component package

    Returns:
        list[str]: Property checks that failed
    """
    repo_url = f"https://api.securityscorecards.dev/projects/{repo_url}"
    response = requests.get(url=repo_url, timeout=30)
    response.raise_for_status()
    scorecard_response = ScorecardResponse(**response.json())

    failed: list[str] = []

    no_border, underline = "    \n", " -  \n"
    simple_box = Box(box=f"{no_border * 2}{underline}{no_border * 5}", ascii=True)
    table = Table("PROPERTY", "VALUE", "EXPECTED", box=simple_box)

    for property_name, scorecard_search in PROPERTY_MAP.items():
        property_search = (
            f"components[? purl == '{purl_string}'] | "
            f"[0].properties[? name == 'hoppr:scorecard:{property_name}'] | "
            "[0].value"
        )

        property_value = str(jmespath.search(expression=property_search, data=delivered_sbom))
        scorecard_value = str(jmespath.search(expression=scorecard_search, data=scorecard_response.dict()))

        table.add_row(property_name, property_value, scorecard_value)

        if property_value != scorecard_value:
            failed.append(property_name)

    rich.print(table)

    return failed


def main():
    """
    Entrypoint for integration test
    """
    test_dir = Path(__file__).parent / sys.argv[1]

    test_data: list[dict[str, Any]] = json.loads((test_dir / "test-data.json").read_text())

    # Extract delivered SBOM from tar bundle
    with tarfile.open(name=test_dir / "tarfile.tar.gz", mode="r:gz") as tar:
        extracted = tar.extractfile(member="./generic/_metadata_/_delivered_bom.json")
        if extracted is None:
            raise IOError("Delivered SBOM file not found in tar bundle.")

        delivered_sbom = json.load(extracted)

    failed = False
    failed_checks: dict[str, list[str]] = {}

    # Submit request against Scorecard API with pre-determined repo URL for comparison
    for data in test_data:
        print(f"\nChecking properties for component '{data['purl']}':\n")

        failed_checks[data["purl"]] = check_result(
            purl_string=data["purl"], delivered_sbom=delivered_sbom, repo_url=data["repo_url"]
        )

        if len(failed_checks[data["purl"]]) > 0:
            failed = True

    # Print summary
    if failed:
        for purl_string, failures in failed_checks.items():
            if len(failures) > 0:
                print(f"Failed checks for '{purl_string}':")
                print("\n".join([f"    {failure}" for failure in failures]))

        sys.exit(1)
    else:
        print("\nAll checks passed!\n")
